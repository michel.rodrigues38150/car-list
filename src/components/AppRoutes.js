import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

let router = new VueRouter({
mode : 'history', //Html 5 API
routes :[
    {
        path:'/',
        component: r => require.ensure([],() => r(require('../components/Navigation.vue').default)),
        name:'navigation'
    },
    {
        path:'/carpage',
        component: require ('../pages/CarPage.vue').default,
        name:'carpage'
    },
    {
        path:'/helloworld',
        component: require ('../components/HelloWorld.vue').default,
        name:'helloworld'
    },
    {
        path:'/hello',
        component:require('../components/Hello.vue').default,
        name:'hello'
    },
    {
        path:'*',
        redirect:'/carpage'
    }
]
})
export default router